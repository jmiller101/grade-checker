// This line of code just makes the parser code available to us.
const Papa = window.Papa;

// Querying the HTML for the file selector
const fileSelector = document.getElementById('file-selector');

// Creating a callback that is triggered whenever the file selector is used
fileSelector.addEventListener('change', event => {
  const fileList = event.target.files;
  console.log(fileList);

  // This is a JavaScript for-each loop
  for (const file of fileList) {
    const reader = new FileReader();

    // Create a callback that is triggered when the FileReader has finished
    // loading a file.
    reader.addEventListener('load', event => {
      const text = event.target.result;
      console.log(text);
      Papa.parse(text, { header: true, complete: handleCsv });
    });

    // Load the file here, which will trigger the event listener that we
    // registered above.
    reader.readAsText(file);
  }
});

// This is where you come in. I've handled setting up the file selector and
// reading the .csv file contents. Now you have access to the data and need
// to traverse it. Here's a link to what the data looks like in the results
// argument:
// https://www.papaparse.com/docs#data
function handleCsv(results) {
  const missing = [];

  for (let row = 0; row < results.data.length; row++) {
    let isMissing = false;

    for (let col = 2; col < Object.keys(results.data[row]).length - 1; col++) {
      if (
        results.data[row]['Assignment ' + (col - 1)] === '0' &&
        results.data[row]['Assignment ' + col] === '0'
      ) {
        isMissing = true;
      }
    }

    if (isMissing) {
      missing.push(results.data[row].Name);
    }
  }

  if (missing.length > 0) {
    download(missing.join('\n'), 'missing-assignments.txt', 'text/plain');
  }
}
