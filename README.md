# grade-checker

## .csv Files

This is super simple. It's just the text representation of a spreadsheet (like
Microsoft Excel or Google Sheets). Both of those programs actually have an
option to save as a .csv file built-in. I've included an example grades file for
you to go off of and start using for testing.

## General JavaScript Stuff

So overall, JavaScript will look similar to other "C-style" language syntaxes
such as Java. For your purposes, you likely won't run into a lot of the issues
listed here! But just so you can understand all of the code that we have in the
`grade-checker.js` file, here's some highlights that can be confusing:

### Single Threaded

JS is always single-threaded. This may not seem like a big deal because it can
be rare to deal with threads anyways. However, there are a lot of languages that
will spin up threads in the background to accomplish things without you
noticing. This is very common when programming user interfaces, for example.

A side effect of this is that if you have an infinite loop in your JS code it
will permanently lock a webpage, because the webpage ALSO uses that same JS
thread! So, what do we do when we need to accomplish large tasks without locking
the browser?

The browser has some asynchronous APIs that we can use to crunch data for us and
then alert us when the task has been complete. There are no guarantees about
when this data will be available though, which leads us to...

### Callbacks

A programming pattern that arose due to the above constraints is called the
callback. Basically, you pass your own function as an argument to the async
process that you're calling and that process will then call the function when
it's done! You no longer have to care about timing or checking for completion.
Whenever the task is complete your code will be called immediately.

There are a few examples of this in our code:

1. We have an event listener on the file selector that calls a function on the
'change' event.
1. We have an event listener on the file reader that calls a function on the
'load' event
1. We have a callback function called handleCsv which will be called when Papa
Parse finished parsing our file.

But wait! Some of these functions don't look right! They don't have names or
anything! That leads us to...

### Anonymous Functions

The short, although not TOTALLY accurate, definition of anonymous function is a
function that doesn't have a name. Basically, there are a few ways of defining a
function in JS. Here they are from most to least verbose:

```
// This is the way that probably looks familiar
function doThing(arg1, arg2) {
  return arg1 + arg2;
}
console.log(doThing(1, 2))

// This is now anonymous! I cannot call it unless it's assigned to something. It
// is effectively dead code.
function(arg1, arg2) {
  return arg1 + arg2;
}

// Now I can call it because I assigned it to a variable
let doThing = function(arg1, arg2) {
  return arg1 + arg2;
}
console.log(doThing(1, 2))

// The "fat arrow" syntax makes the code more concise
let doThing = (arg1, arg2) => {
  return arg1 + arg2;
}
console.log(doThing(1, 2))

// You can even remove the parens around the argument of a fat arrow function
// when you only have 1
let doThing = arg1 => {
  return arg1 * 2;
}
console.log(doThing(2))
```

Anonymous functions are nice because a lot of callbacks and listeners aren't
ever used in more than one place. There's no point in giving them names and
stuff if it's only ever used in one spot. Also, the shortened syntax is nice
because it allows us to just define the function right where it will be used.
